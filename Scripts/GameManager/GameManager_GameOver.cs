﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class GameManager_GameOver : MonoBehaviour {
		private GameManager_Master gameManager;
		public GameObject gameOverPanel;

		void OnEnable() {
			SetInitialReferences();
			gameManager.GameOverEvent += TurnOnGameOverPanel;
		}

		void OnDisable() {
			gameManager.GameOverEvent -= TurnOnGameOverPanel;
		}

		void SetInitialReferences() {
			gameManager = GetComponent<GameManager_Master>();
		}

		void TurnOnGameOverPanel() {
			if (gameOverPanel != null) {
				gameOverPanel.SetActive(!gameOverPanel.activeSelf);
				gameManager.isGameOver = !gameManager.isGameOver;
			}
		}
	}
}