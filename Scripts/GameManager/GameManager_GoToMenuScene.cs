﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GeoffEngine {
	public class GameManager_GoToMenuScene : MonoBehaviour {
		private GameManager_Master gameManager;

		void OnEnable() {
			SetInitialReferences();
			gameManager.GoToMenuSceneEvent += GoToMenuScene;
		}

		void OnDisable() {
			gameManager.GoToMenuSceneEvent -= GoToMenuScene;
		}

		void SetInitialReferences() {
			gameManager = GetComponent<GameManager_Master>();
		}

		void GoToMenuScene() {
			SceneManager.LoadScene("Main Menu");
		}
	}
}