﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class GameManager_Master : MonoBehaviour {

		// Events
		public delegate void GameManagerEventHandler();
		public event GameManagerEventHandler MenuToggleEvent;
		public event GameManagerEventHandler InventoryUIToggleEvent;
		public event GameManagerEventHandler RestartLevel;
		public event GameManagerEventHandler GoToMenuSceneEvent;
		public event GameManagerEventHandler GameOverEvent;

		public bool isGameOver;
		public bool isIventoryUIOn;
		public bool isMenuOn;

		public void CallEventMenuToggle() {
			if (MenuToggleEvent != null) {
				MenuToggleEvent();
			}
		}

		public void CallInventoryUIToggleEvent() {
			if (InventoryUIToggleEvent != null) {
				InventoryUIToggleEvent();
			}
		}

		public void CallRestartLevel() {
			if (RestartLevel != null) {
				RestartLevel();
			}
		}

		public void CallGoToMenuSceneEvent() {
			if (GoToMenuSceneEvent != null) {
				GoToMenuSceneEvent();
			}
		}

		public void CallGameOverEvent() {
			if (GameOverEvent != null) {
				GameOverEvent();
			}
		}
	}	
}
