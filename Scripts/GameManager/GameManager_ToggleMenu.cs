﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class GameManager_ToggleMenu : MonoBehaviour {

		private GameManager_Master gameManager;
		public GameObject menu;

		void Start() {
			ToggleMenu();
		}

		void Update() {
			CheckForMenuToggleRequest();
		}

		void OnEnable() {
			SetInitialReferences();
			gameManager.GameOverEvent += ToggleMenu;
		}

		void OnDisable() {
			gameManager.GameOverEvent -= ToggleMenu;
		}

		void SetInitialReferences() {
			gameManager = GetComponent<GameManager_Master>();
		}

		void CheckForMenuToggleRequest() {
			if (Input.GetKeyDown(KeyCode.At) && !gameManager.isGameOver && !gameManager.isIventoryUIOn) {
				ToggleMenu();
			}
		}

		void ToggleMenu() {
			if (menu != null) {
				menu.SetActive(!menu.activeSelf);
				gameManager.isMenuOn = !gameManager.isMenuOn;
				gameManager.CallEventMenuToggle();
			}
		}
	}
}