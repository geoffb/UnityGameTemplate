﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class GameManager_TogglePause : MonoBehaviour {
		private GameManager_Master gameManager;
		private bool isPaused;

		void OnEnable() {
			SetInitialReferences();
			gameManager.MenuToggleEvent += TogglePause;
			gameManager.InventoryUIToggleEvent += TogglePause;
		}

		void OnDisable() {
			gameManager.MenuToggleEvent -= TogglePause;
			gameManager.InventoryUIToggleEvent -= TogglePause;
		}

		void SetInitialReferences() {
			gameManager = GetComponent<GameManager_Master>();
		}

		void TogglePause() {
			if (isPaused) {
				Time.timeScale = 1;
				isPaused = false;
			}
			else {
				Time.timeScale = 0;
				isPaused = true;
			}
		}
	}
}