﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class GameManager_ToggleCursor : MonoBehaviour {
		private GameManager_Master gameManager;
		private bool isCursorLocked = true;

		// Use this for initialization
		void OnEnable () {
			SetInitialReferences();
			gameManager.MenuToggleEvent += ToggleCursorState;
			gameManager.InventoryUIToggleEvent += ToggleCursorState;
		}

		void OnDisable () {
			gameManager.MenuToggleEvent -= ToggleCursorState;
			gameManager.InventoryUIToggleEvent -= ToggleCursorState;
		}
		
		// Update is called once per frame
		void Update () {
			CheckIfCursorShouldBeLocked();
		}

		void SetInitialReferences() {
			gameManager = GetComponent<GameManager_Master>();
		}

		void ToggleCursorState() {
			isCursorLocked = !isCursorLocked;
		}

		void CheckIfCursorShouldBeLocked() {
			if (isCursorLocked) {
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}
			else {
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
			}
		}
	}
}