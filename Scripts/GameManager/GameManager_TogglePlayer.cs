﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

namespace GeoffEngine {
	public class GameManager_TogglePlayer : MonoBehaviour {

		public FirstPersonController fpsController;
		private GameManager_Master gameManager;

		void OnEnable () {
			SetInitialReferences();
			gameManager.MenuToggleEvent += TogglePlayerController;
			gameManager.InventoryUIToggleEvent += TogglePlayerController;
		}
		
		void OnDisable () {
			gameManager.MenuToggleEvent -= TogglePlayerController;
			gameManager.InventoryUIToggleEvent -= TogglePlayerController;
		}

		void SetInitialReferences() {
			gameManager = GetComponent<GameManager_Master>();
		}

		void TogglePlayerController() {
			if (fpsController != null) {
				fpsController.enabled = !fpsController.enabled;
			}
		}
	}
}